# Licensing #

Please read this complete file before copying or using the code anywhere. Questions can be conveyed by raising an
issue.

The code written for this project is tri-licensed, under the CC0, MIT and Apache 2.0 licenses.

This means that you can select and use one of these licenses, i.e. CC0 or MIT or Apache 2.0. You do not have to
use all three licenses at once.

Clear attribution is strongly preferred (but not enforced by CC0).

The code was originally licensed under the MIT license only. I sought and received permission from the other two
contributors (Robin Eklind, a.k.a 'mewmew', and 'ChaosMarc') to re-license the code base (personal communication).

The code under the 'knowledge' package is NOT covered by these licenses. The knowledge package contains strings extracted
from the game and descriptions written by those who created the Mod Workshop (Charlie with assistance from Jarulf) as
hosted by Zamal & Zenda. The strings are very simple, such as "Leather Armor" and I am uncertain exactly what the
copyright position is on these strings. The data stored is no greater than that contained within any Diablo 1 game
guide (actually, far less). If any copyright holder objects to the use herein, please raise an issue,
and we will remedy the problem and then try to work around the issue created (though it may well mean the
editor will have to be discontinued).

This project pulls in additional code via Maven. This code is not covered by the above license(s). Each dependency
pulled in is subject to its own license. Please see the README.md file.

